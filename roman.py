def roman_numerals_to_int(roman_num):
    if check_input(roman_num):
        roman = {'M': 1000, 'CM': 900, 'D': 500, 'CD': 400, 'C': 100, 'XC': 90, 'L': 50, 'XL': 40,
                 'X': 10, 'IX': 9, 'V': 5, 'IV': 4, 'I': 1}
        length = 0
        res = 0
        current = 1000
        while length < len(roman_num):
            for key, value in roman.items():
                if key == roman_num[length:length + 2]:
                    if value <= current:
                        res += value
                        current = value
                        length += 2
                        break
                    elif value > current:
                        # print("число не соответствует нотации")
                        return
                elif key == roman_num[length]:
                    if value <= current:
                        res += value
                        current = value
                        length += 1
                        break
                    elif value > current:
                        # print("число не соответствует нотации")
                        return
            if len(roman_num) < length:
                length -= 1
        if res > 3999:
            print(
                '')  # максимальное число, которое можно получить - 3999, другие значения конвертируются в иной нотации
        print(roman_num, ' ', res)
        return res
    else:
        # print("число не соответствует нотации")
        return


def check_input(roman: str):
    s = roman.find('CCCC')
    if roman.find('MMMM') != -1 or roman.find('CCCC') != -1 or roman.find(
            'XXXX') != -1 or roman.find('IIII') != -1 or roman.find(
        'CMC') != -1 or roman.find(
        'CDC') != -1 or roman.find('XCX') != -1 or roman.find('XLX') != -1 or roman.find('IXI') != -1:
        return False
    else:
        return True
