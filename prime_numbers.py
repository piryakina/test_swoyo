import math


def prime_numbers(low, high):
    if low < 0 or high < 0 or low >= high:
        # print([])
        return []
    else:
        primes = atkin(high)
        for i in primes:
            if i >= low:
                primes = primes[primes.index(i):len(primes)]
                break
        # print(primes)
        return primes


def atkin(num):
    sqr_lim = int(math.sqrt(num))  # ближайшее целое число для корня из high
    is_prime = [False] * (num + 1)  # матрица,  в которой будут выделены простые числа
    is_prime[2] = True  # числа 2 и 3 заведомо простые
    is_prime[3] = True
    result = []
    x2 = 0
    # x2 и y2 — это квадраты i и j(оптимизация).
    for i in range(1, sqr_lim + 1):
        x2 += 2 * i - 1
        y2 = 0
        for j in range(1, sqr_lim + 1):
            y2 += 2 * j - 1
            n = 4 * x2 + y2
            if (n <= num) and (n % 12 == 1 or n % 12 == 5):
                is_prime[n] = not is_prime[n]
            n -= x2
            if (n <= num) and (n % 12 == 7):
                is_prime[n] = not is_prime[n]
            n -= 2 * y2
            if (i > j) and (n <= num) and (n % 12 == 11):
                is_prime[n] = not is_prime[n]
    for i in range(5, sqr_lim + 1):
        if is_prime[i]:
            n = i * i
            for j in range(n, num + 1, n):
                is_prime[j] = False
    for i in range(0, len(is_prime)):
        if is_prime[i]:
            result.append(i)
    return result
