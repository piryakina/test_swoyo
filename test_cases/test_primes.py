import unittest
from prime_numbers import prime_numbers


class TestPrimes(unittest.TestCase):
    def test_positive_10(self):
        self.assertEqual(prime_numbers(1, 10), [2, 3, 5, 7])

    def test_positive_100(self):
        self.assertEqual(prime_numbers(10, 100),
                         [11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89,
                          97])

    def test_negative_1(self):
        self.assertEqual(prime_numbers(1, -1010), [])

    def test_negative_2(self):
        self.assertEqual(prime_numbers(1, 1), [])

    def test_negative_3(self):
        self.assertEqual(prime_numbers(-1, 0), [])

    def test_negative_4(self):
        self.assertNotIn([4, 6, 8, 9], prime_numbers(1, 10))

    def test_negative_5(self):
        self.assertNotIn([50, 66, 80], prime_numbers(1, 100))
