import unittest
from roman import roman_numerals_to_int


class TestRomans(unittest.TestCase):
    def test_positive_1(self):
        self.assertEqual(roman_numerals_to_int('III'), 3)

    def test_positive_2(self):
        self.assertEqual(roman_numerals_to_int('VIII'), 8)

    def test_positive_3(self):
        self.assertEqual(roman_numerals_to_int('XXIII'), 23)

    def test_positive_4(self):
        self.assertEqual(roman_numerals_to_int('MMMCMXCIX'), 3999)

    def test_positive_5(self):
        self.assertEqual(roman_numerals_to_int('MMMCCCLLLXXXVVVIII'), 3498)

    def test_negative_1(self):
        self.assertIsNone(roman_numerals_to_int('MMMM'))

    def test_negative_2(self):
        self.assertIsNone(roman_numerals_to_int('MCMCC'))

    def test_negative_3(self):
        self.assertEqual(roman_numerals_to_int(''), 0)

    def test_negative_4(self):
        self.assertEqual(roman_numerals_to_int(''), 0)
