﻿# ignore_list = [',', '.', '-', '\t']
delimiters = {
    'space': [' '],
    'paragraph': ['\t']
}
find_list_eng = tuple(chr(i) for i in range(97, 123))
find_list_rus = tuple(chr(i) for i in range(1072, 1104))
find_list_rus_up = tuple(map(lambda a: a.upper(), find_list_rus))
find_list_eng_up = tuple(map(lambda a: a.upper(), find_list_eng))
abc = find_list_eng + find_list_eng_up + find_list_rus + find_list_rus_up
abc_dictionary = {symbol: {'count': 0, 'fraction': 0} for symbol in abc}
billingual_letters_en = ('A', 'a', 'B', 'C', 'c', 'E', 'e', 'H', 'K', 'M', 'O', 'o', 'P', 'p', 'T', 'X', 'x', 'y')
billingual_letters_ru = ('А', 'а', 'В', 'С', 'с', 'Е', 'е', 'Н', 'К', 'М', 'О', 'о', 'Р', 'р', 'Т', 'Х', 'х', 'у')
billingual = set.union(set(billingual_letters_ru), set(billingual_letters_en))


def clear_current_word(current_word, stat):
    if billingual.intersection(set(current_word)):
        stat['billingual_word_amount'] += 1
    for i in set(current_word):
        abc_dictionary[i]['fraction'] += 1
    stat['word_amount'] += 1
    return ''


def text_stat(filename):
    f = open(filename, 'rt', encoding="utf8")
    if f:
        stat = {
            'word_amount': 0,
            'paragraph_amount': 0,
            'billingual_word_amount': 0,
        }
        letters = 0
        current_word = ''
        for symbol in f.read():
            if symbol in abc:
                abc_dictionary[symbol]['count'] += 1
                current_word += symbol
                letters += 1
            elif symbol in delimiters['space']:
                current_word = clear_current_word(current_word, stat)
            elif symbol in delimiters['paragraph']:
                stat['paragraph_amount'] += 1
                if len(current_word) != 0:
                    current_word = clear_current_word(current_word, stat)
        if current_word != '':
            clear_current_word(current_word, stat)
        for sym in abc_dictionary:
            stat[sym] = (round(abc_dictionary[sym]['count'] / letters, 4),
                         round(abc_dictionary[sym]['fraction'] / stat['word_amount'], 4))
    else:
        stat = {
            'error': 'проблемы с открытием файла'
        }
    f.close()
    return stat
